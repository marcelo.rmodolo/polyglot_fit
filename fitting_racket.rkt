#lang racket

; Copyright (c) 2019 Cristiano L. Fontana
; 
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
; 
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
; 
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.

(require csv-reading)
(require bestfit)
(require math/statistics)
(require plot/no-gui)

(define input-file-name "anscombe.csv")
(define column-x 0)
(define column-y 1)

(displayln "#### Anscombe's first set with Racket ####")

(define (get-x-and-y row)
  (cons (list-ref row column-x) (list-ref row column-y)))

(define data (csv-map get-x-and-y
                      (make-csv-reader
                       (open-input-file input-file-name)
                       '((separator-chars "\t")
                         (comment-chars "#")
                         (strip-leading-whitespace? . #t)
                         (strip-trailing-whitespace? . #t)))))

(define x (map (λ (datum)
                 (string->number (car datum)))
               data))
(define y (map (λ (datum)
                 (string->number (cdr datum)))
               data))

(define min-x (apply min x))
(define max-x (apply max x))
(define min-y (apply min y))
(define max-y (apply max y))

(define-values (intercept slope) (linear-fit-params x y))
(define r-value (correlation x y))

(displayln (string-append "Slope: " (~r slope)))
(displayln (string-append "Intercept: " (~r intercept)))
(displayln (string-append "Correlation coefficient: " (~r r-value)))

(define fig-dpi 100)
(define fig-width (* 7 fig-dpi))
(define fig-height (floor (* (/ fig-width 16) 9)))
(define image-file-name "fit_racket.png")

(plot-width   fig-width)
(plot-height  fig-height)
(plot-x-label "x")
(plot-y-label "y")

(plot-file (list (function (λ (x) (+ (* slope x) intercept))
                           (- min-x 1) (+ max-x 1)
                           #:style 'solid
                           #:label "Fit")
                 (points (map vector x y)
                         #:y-min (- min-y 1) #:y-max (- max-y 1)
                         #:label "Data"))
           image-file-name
           'png)

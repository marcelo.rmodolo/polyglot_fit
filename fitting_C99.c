// Copyright (c) 2019 Cristiano L. Fontana
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_statistics_double.h>

struct data_point {
    double x;
    double y;

    SLIST_ENTRY(data_point) entries;
};
 
int main(void) {

    const char *input_file_name = "anscombe.csv";
    const char *delimiter = "\t";
    const unsigned int skip_header = 3;
    const unsigned int column_x = 0;
    const unsigned int column_y = 1;
    const char *output_file_name = "fit_C99.csv";
    const unsigned int N = 100;

    SLIST_HEAD(data_list, data_point) head = SLIST_HEAD_INITIALIZER(head);
    SLIST_INIT(&head);

    printf("#### Anscombe's first set with C99 ####\n");

    FILE* input_file = fopen(input_file_name, "r");

    if (!input_file) {
        printf("ERROR: Unable to open file: %s", input_file_name);

        return EXIT_FAILURE;
    }

    unsigned int row = 0;

    while (!ferror(input_file) && !feof(input_file)) {
        size_t buffer_size = 0;
        char *buffer = NULL;
        
        getline(&buffer, &buffer_size, input_file);

        if (row >= skip_header && strlen(buffer) > 0) {
            double x = 0;
            double y = 0;
            unsigned int column = 0;

            char *token = strtok(buffer, delimiter);

            while (token != NULL)
            {
                double value;
                sscanf(token, "%lf", &value);

                if (column == column_x) {
                    x = value;
                } else if (column == column_y) {
                    y = value;
                }

                column += 1;
                token = strtok(NULL, delimiter);
            }

            struct data_point *datum = malloc(sizeof(struct data_point));
            datum->x = x;
            datum->y = y;

            SLIST_INSERT_HEAD(&head, datum, entries);
        }

        free(buffer);

        row += 1;
    }

    fclose(input_file);

    const size_t entries_number = row - skip_header - 1;

    double *x = malloc(sizeof(double) * entries_number);
    double *y = malloc(sizeof(double) * entries_number);

    if (!x || !y) {
        printf("ERROR: Unable to allocate data arrays\n");

        return EXIT_FAILURE;
    }

    double min_x, max_x;

    struct data_point *datum;
    unsigned int i = 0;

    datum = SLIST_FIRST(&head);

    min_x = datum->x;
    max_x = datum->x;

    SLIST_FOREACH(datum, &head, entries) {
        const double current_x = datum->x;
        const double current_y = datum->y;

        x[i] = current_x;
        y[i] = current_y;

        if (current_x < min_x) {
            min_x = current_x;
        }
        if (current_x > max_x) {
            max_x = current_x;
        }

        i += 1;
    }

    while (!SLIST_EMPTY(&head)) {
        struct data_point *datum = SLIST_FIRST(&head);

        SLIST_REMOVE_HEAD(&head, entries);

        free(datum);
    }

    double slope;
    double intercept;
    double cov00, cov01, cov11;
    double chi_squared;

    gsl_fit_linear(x, 1, y, 1, entries_number,
                   &intercept, &slope,
                   &cov00, &cov01, &cov11, &chi_squared);
    const double r_value = gsl_stats_correlation(x, 1, y, 1, entries_number);

    printf("Slope: %f\n", slope);
    printf("Intercept: %f\n", intercept);
    printf("Correlation coefficient: %f\n", r_value);

    FILE* output_file = fopen(output_file_name, "w");

    if (!output_file) {
        printf("ERROR: Unable to open file: %s", output_file_name);

        return EXIT_FAILURE;
    }

    const double step_x = ((max_x + 1) - (min_x - 1)) / N;

    for (unsigned int i = 0; i < N; i += 1) {
        const double current_x = (min_x - 1) + step_x * i;
        const double current_y = intercept + slope * current_x;

        fprintf(output_file, "%f\t%f\n", current_x, current_y);
    }

    fclose(output_file);

    return EXIT_SUCCESS;
}

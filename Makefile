LIBS += -lgsl -lgslcblas

CC = clang

CFLAGS += -std=c99 -W -Wall -pedantic -I/usr/include/
LDFLAGS += -L/usr/lib/ -L/usr/lib64/ $(LIBS)

CXX = clang++

CXXFLAGS += -std=c++11 -W -Wall -pedantic -I/usr/include/

.PHONY: all clean $(MODULES)

all: fit_C99.png fit_Cpp11.png fit_python.png fit_octave.png fit_racket.png fit_node.svg fit_R.png

fitting_C99: fitting_C99.c
	$(CC) $(CFLAGS) $< $(LDFLAGS) -o $@

fit_C99.csv: fitting_C99
	./fitting_C99

fit_C99.png: fit_C99.csv
	gnuplot plotting_C99.gnuplot

fitting_Cpp11: fitting_Cpp11.cpp
	$(CXX) $(CXXFLAGS) $< $(LDFLAGS) -o $@

fit_Cpp11.csv: fitting_Cpp11
	./fitting_Cpp11

fit_Cpp11.png: fit_Cpp11.csv
	gnuplot plotting_Cpp11.gnuplot

fit_python.png: fitting_python.py
	./fitting_python.py

fit_octave.png: fitting_octave.m
	./fitting_octave.m

fit_racket.png: fitting_racket.rkt
	racket fitting_racket.rkt

fit_node.svg: fitting_node.js
	node fitting_node.js

fit_R.png: fitting_R.r
	Rscript fitting_R.r

clean:
	rm -f fitting_C99 fit_C99.csv fit_C99.png fitting_Cpp11 fit_Cpp11.csv fit_Cpp11.png fit_python.png fit_octave.png fit_racket.png fit_node.svg fit_R.png
